/**
 * 公共方法
 * */
/*  格式化 年月日 */
export function getCurrentDate(nS) {
  const date = nS ? new Date(nS) : new Date();
  const year = date.getFullYear();
  const mon = date.getMonth() + 1;
  const monStr = mon > 9 ? mon : `0${mon}`;
  const day = date.getDate();
  const dayStr = day > 9 ? day : `0${day}`;
  return `${year}-${monStr}-${dayStr}`;
}

/*  格式化 时分秒 */
export function getCurrentHouser(nS, bool:Boolean) {
  const date = nS ? new Date(nS) : new Date();
  let Hours = date.getHours();
  let Minutes = date.getMinutes();
  let Seconds = date.getSeconds();
  const HourStr = Hours > 9 ? Hours : `0${Hours}`;
  const MinuteSrc = Minutes > 9 ? Minutes : `0${Minutes}`;
  const SecondSrc = Seconds > 9 ? Seconds : `0${Seconds}`;
  if (bool) {
    return HourStr;
  } else {
    return `${HourStr}:${MinuteSrc}:${SecondSrc}`;
  }
}

/*  格式化 时 */
export const excelDown = (blob, name) => {
  const objectUrl = URL.createObjectURL(blob);
  const a = document.createElement('a');
  document.body.appendChild(a);
  a.style.display = 'none';
  a.href = objectUrl;
  a.download = name;
  a.click();
  document.body.removeChild(a);
};

// 格式化数字   列 formatMoney(5000,2)  =>   5,000.00
export const formatMoney = (s) => {
  if (!s) {
    return '0.00';
  }
  let n = 2;
  let isFilter = false;
  if (n) {
    n = n > 0 && n <= 20 ? n : 2;
    s = `${parseFloat(String(s).replace(/[^\d.-]/g, '')).toFixed(n)}`;
  } else {
    s = parseInt(String(s).replace(/[^\d.-]/g, ''));
  }

  const l = s.split('.')[0].split('').reverse();
  const r = s.split('.')[1];
  let t = '';
  /*  过滤负数 -号 */
  if (l.indexOf('-') !== -1) {
    l.splice(l.indexOf('-'), 1);
    isFilter = true;
  }
  /* end */
  for (let i = 0; i < l.length; i++) {
    t += l[i] + ((i + 1) % 3 == 0 && i + 1 != l.length ? ',' : '');
  }
  if (!isFilter) {
    return `${t.split('').reverse().join('')}.${r}`;
  } else {
    return `-${t.split('').reverse().join('')}.${r}`;
  }
};

// 数组对象去重 arr 要去重的数组 默认用对象的 id 作为标识去重
export function repeatObj(arr, args) {
  const result = [];
  const arg = args || 'id';
  const obj = {};
  for (const item in arr) {
    if (!obj[arr[item][arg]]) {
      result.push(arr[item]);
      obj[arr[item][arg]] = true;
    }
  }
  return result;
}

/**
 * 树数据过滤
 * @param tree 树数据
 * @param childKey 子数据key
 * @param filterKey 需要筛选的key
 * @param value 需要筛选的值
 */
export const filterTreeNode = (tree, childKey, filterKey, value) => {
  let newArr = [];
  for (let item of tree) {
    if (item[childKey] && item[childKey].length) {
      item[childKey] = filterTreeNode(
        item[childKey],
        childKey,
        filterKey,
        value
      );
    }

    let res = item[filterKey];
    if (res === value) {
      newArr.push(item);
    }
  }
  return newArr;
};


/**
 * 防抖函数
 * @param {*} func 传入方法
 * @param {*} delay 延迟时间
 */
export const debounce = (func, delay) => {
  let timer;
  return function (...args) {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      func.apply(this, args);
    }, delay);
  };
};


export function isArr(obj) {
  return obj && Object.prototype.toString.call(obj) === '[object Array]';
}
export const deleteObjKeys = (obj, key) => {
  if (typeof key === 'string') {
    return delete obj[key];
  }
  if (isArr(key)) {
    return key.forEach((item) => {
      delete obj[item];
    });
  }
};

export function moneyUnitSwitch(value:number, baseUnit:number, NaNText) {
  if (isNaN(value)) {
    return NaNText || 0;
  }
  baseUnit = baseUnit || 10000;
  let num = (value / baseUnit).toFixed(2);
  const numStr = num.toString();

  // 千分位符
  const strZS = numStr.split('.')[0];
  const strXS = numStr.split('.').length > 1 ? `.${numStr.split('.')[1]}` : '';
  const strArr = `${strZS}`
    .split('')
    .reverse()
    .join('')
    .replace(/(\d{3})(?=\d)/g, '$1,')
    .split('')
    .reverse();
  return strArr.join('') + strXS;
}

export function getUrlParam(paraName) {
  const url = document.location.toString();
  const arrObj = url.split('?');

  if (arrObj.length > 1) {
    const arrPara = arrObj[1].split('&');
    let arr;

    for (let i = 0; i < arrPara.length; i++) {
      arr = arrPara[i].split('=');

      if (arr != null && arr[0] == paraName) {
        return arr[1];
      }
    }
    return '';
  } else {
    return '';
  }
}

// num为传入的值，n为保留的小数位 n 默认保留2位小数
export function formatFloat(num, n) {
  let f = parseFloat(num);
  if (isNaN(f)) {
    return 0;
  }
  n = n || 2;
  f = Math.round(num * Math.pow(10, n)) / Math.pow(10, n); // n 幂
  let s = f.toString();
  let rs = s.indexOf('.');
  // 判定如果是整数，增加小数点再补0
  if (rs < 0) {
    rs = s.length;
    s += '.';
  }
  while (s.length <= rs + n) {
    s += '0';
  }
  return s;
}


// 只能输入数字和小数点；
// 小数点只能有1个
// 第一位不能是小数点 也不能是 0
// 小数点后保留2位
export function numberCheck(num) {
  console.log('num',num)
  let str = num.toString();
  const len1 = str.substr(0, 1);
  const len2 = str.substr(1, 1);
  console.log('len1',len1)
  console.log('len2',len2)
  console.log('str.length ',str.length )
  // 如果第一位是0，第二位不是点，就用数字把点替换掉
  if (str.length > 1 && len1 == 0 && len2 != '.') {
    str = str.substr(1, 1);
  }
  // 第一位不能是.
  if (len1 == '.') {
    str = '';
  }
  // 限制只能输入一个小数点
  if (str.indexOf('.') != -1) {
    const str_ = str.substr(str.indexOf('.') + 1);
    if (str_.indexOf('.') != -1) {
      str = str.substr(0, str.indexOf('.') + str_.indexOf('.') + 1);
    }
  }
  // 正则替换，保留数字和小数点
  str = str.replace(/[^\d.]/g, '');
  // 如果需要保留小数点后两位，则用下面公式
  str = str.replace(/\.\d\d\d$/, '');
  /* 特殊处理 */
  // if (str) {
  //   return +str;
  // }else{
  //   return null;
  // }
  /* end */
  /* 正常处理 */
  return str;
}


// 只能输入整数 第一位不能为0
export function filterNum(num) {
  let str = num.replace(/[^\d]/g, '');
  str = str.toString();
  if (str.substr(0, 1) == 0) {
    return '';
  }
  return str;
}


// 只能输入整数
export function filterNumber(num) {
  let str = num.replace(/[^\d]/g, '');
  str = str.toString();
  return str;
}
// 限制输入逗号
export function strIpScript(value) {
  return value.replace(',', '');
}
/**
 *  权限对比
 *  @param  matchArr
 *  @param  userArr
 *  @returns  {boolean}
 */
export function authorityComparison(matchArr, userArr) {
  if (!userArr) {
    // 如果没有设置权限码，默认不需要
    return true;
  }
  let temp = false;
  matchArr = matchArr || [];
  if (typeof userArr === 'string' || typeof userArr === 'number') {
    return matchArr.includes(userArr);
  } else {
    userArr.forEach((item) => {
      temp = temp || matchArr.includes(item);
    });
  }
  return temp;
}
